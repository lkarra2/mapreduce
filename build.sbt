lazy val Settings = Seq(
  name := "lakshmi_manaswi_karra_cs441_hw2",
  version := "0.1",
  scalaVersion := "2.12.8",
  test in assembly := {}
)

lazy val app = (project in file("lakshmi_manaswi_karra_cs441_hw2")).
  settings(Settings: _*).
  settings(
    mainClass in assembly := Some("XmlDriver")
    // more settings here ...
  )

assemblyMergeStrategy in
  assembly := { case PathList("META-INF", xs @ _*) => MergeStrategy.discard
case x => MergeStrategy.first }

libraryDependencies ++= Seq(
  //"org.apache.hadoop" % "hadoop-mapreduce" % "3.2.0",
  "org.apache.hadoop" % "hadoop-core" % "1.2.1",
  //"org.apache.hadoop" % "hadoop-common" % "2.7.2",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2",
  "ch.qos.logback" % "logback-classic" % "1.2.3",
  "org.scala-lang.modules" %% "scala-xml" % "1.1.1",
  "org.scalatest" % "scalatest_2.12" % "3.0.5" % "test",
  "junit" % "junit" % "4.12" % Test,
  //"org.apache.hadoop" % "hadoop-mapreduce-client-core" % "2.7.2",
  //"org.apache.hadoop" % "hadoop-mapreduce-client-shuffle" % "2.3.0"
)