import java.io.File
import org.apache.commons.logging.LogFactory
import org.junit.Test

class UtilitiesTest {
  /**
    * Class of tests to ensure Utilities is working as it should
    */
  private val logger = LogFactory.getLog(classOf[MyMapper])


  @Test def authorNameParserTestNotEmpty(): Unit = {
    val authors = Utilities.authorNameParser()
    assert(authors.size() != 0)
  }

  @Test def authorNameParserTestSize(): Unit = {
    val authors = Utilities.authorNameParser()
    assert(authors.size() == 50)
  }

  @Test def authorNameParserTestValues(): Unit = {
    val authors = Utilities.authorNameParser()
    authors.forEach(a => {
      if(a.contains("Mark Grachanik"))
        assert(true)
      if(a.contains("Ugo Buy"))
        assert(true)
    })
  }

  @Test def authorNameParserTestDualNameChack(): Unit = {
    val authors = Utilities.authorNameParser()
    authors.forEach(a => {
      if(a.contains("A. Prasad Sistla") && a.contains("Prasad Sistla"))
        assert(true)
      if(a.contains("Ugo Buy") && a.contains("Ugo A. Buy"))
        assert(true)
    })
  }

  @Test def createCSVTestNotEmpty(): Unit = {
    assert((new File(Utilities.outputFolder + "/graph.csv" ).exists()) == false)
  }
}