import java.util.Calendar

import org.apache.hadoop.conf._
import org.apache.hadoop.io._
import org.apache.hadoop.mapreduce._
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat
import org.slf4j.LoggerFactory
import org.apache.hadoop.fs.{Path}


object XmlMapReduce {
  /** Krishna - for processing XML file using Hadoop MapReduce
    * Lakshmi Manaswi Karra - Modiefied to be used in Scala for CS441 hw2 assignment
    * The main function of the XML Map Reduce Class
    * Loads the configuration, creates Job and sets Mapper, Reducer and InputFormat classes
    */
  val logger = LoggerFactory.getLogger(getClass)

  def main(args: Array[String]): Unit = {
    if(args.length > 0)
      Utilities.inputFolder = args(0)
    if(args.length > 1)
      Utilities.outputFolder = args(1)

    //TODO Add <www> <inproceedings> etc to MyMapper
    //TODO Add comments
    //TODO Write tests
    try {
      logger.debug(""+Calendar.getInstance().getTime())
      val conf = new Configuration(true)
      conf.setInt("mapreduce.input.fixedlengthinputformat.record.length", 2048);
      conf.set("mapreduce.framework.name", "localhost")
      conf.set("START_TAG_KEY", "<dblp>")
      conf.set("END_TAG_KEY", "</dblp>")
      val job = Job.getInstance(conf, "XML Processing Processing")
      job.setJarByClass(classOf[XmlMapReduce])
      job.setMapperClass(classOf[MyMapper])
      job.setReducerClass(classOf[MyReducer])
      job.setInputFormatClass(classOf[XmlInputFormat])
      job.setOutputValueClass(classOf[IntWritable]);
      job.setMapOutputKeyClass(classOf[Text])
      FileInputFormat.addInputPath(job, new Path(Utilities.inputFolder))
      FileInputFormat.setMaxInputSplitSize(job, 1280000000)
      //TODO Check if out dir exists, delete if it does
      FileOutputFormat.setOutputPath(job, new Path(Utilities.outputFolder))
      logger.debug("Args 0:" + Utilities.inputFolder)
      logger.debug("Args 1:" + Utilities.outputFolder)
      job.waitForCompletion(true)
      logger.debug(""+Calendar.getInstance().getTime())
      Utilities.createCSV()
    } catch {
      case e: Exception =>
        System.out.println("Driver Error: " + e.getMessage)
        System.out.println(e.getMessage.toString)
    }
  }
}

class XmlMapReduce {

}