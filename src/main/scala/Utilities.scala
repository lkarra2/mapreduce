import java.io._
import org.slf4j.LoggerFactory
import scala.io.Source


object Utilities {
  /**
    * A helper object consisting of functions to help the Mapper and Reducer
    */
  val logger = LoggerFactory.getLogger(getClass)
  // Setting default input and output folders
  var inputFolder = "input"
  var outputFolder = "output"

  // Function to convert the text input file professorNames.txt into ArrayList[ArrayList[Professor Names]]
  def authorNameParser(): java.util.ArrayList[Array[String]] = {
    val authors = new java.util.ArrayList[Array[String]]
    for (line <- Source.fromResource("professorNames.txt").getLines) {
      val columns = line.split(",").map(_.trim)
      authors.add(columns)
    }
    authors
  }

  // Function to convert generated output file into CSV file necessary for Graph creation in Gephi
  def createCSV(): Unit = {
    // FileWriter
    val printWriter = new PrintWriter(new File(Utilities.outputFolder + "/graph.csv" ))
    val filename = Utilities.outputFolder + "/part-r-00000"
    for (line <- Source.fromFile(filename).getLines) {
      val columns = line.split("\t").map(_.trim)
      val range = 0 until columns(2).toInt
      range.foreach(i=>{
        printWriter.write(columns(0)+","+columns(1)+"\n")
      })
    }
    printWriter.close()
  }


}
