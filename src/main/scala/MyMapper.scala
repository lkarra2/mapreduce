
import java.io.ByteArrayInputStream
import java.io.IOException
import java.util._
import javax.xml.parsers.DocumentBuilderFactory
import org.apache.commons.logging.LogFactory
import org.apache.hadoop.io.{IntWritable, LongWritable, Text}
import org.apache.hadoop.mapreduce.Mapper
import org.w3c.dom.{Element, Node, NodeList}

object MyMapper {
}

class MyMapper extends Mapper[LongWritable, Text, Text, IntWritable] {
  /**
    * Mapper Class
    *
    */
  private val logger = LogFactory.getLog(classOf[MyMapper])

  // Val to store list of Professor Names
  val uicProfList = Utilities.authorNameParser()

  @throws[IOException]
  @throws[InterruptedException]
  override def map(key: LongWritable, value: Text, context: Mapper[LongWritable, Text, Text, IntWritable]#Context): Unit = {
    try {
      val text = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n<!DOCTYPE dblp SYSTEM \""+ "/Users/manaswikarra/CloudComputing/dblp.dtd"+"\">\n<dblp>\n"+value.toString+"\n</dblp>"
      val is = new ByteArrayInputStream(text.getBytes)
      //val is = new ByteArrayInputStream(value.toString.getBytes)
      val dbFactory = DocumentBuilderFactory.newInstance
      val dBuilder = dbFactory.newDocumentBuilder
      val doc = dBuilder.parse(is)
      doc.getDocumentElement.normalize()
      val nList = doc.getElementsByTagName("article")
      //val mList = doc.getElementsByTagName("www")
      writeContext(context, nList)
    } catch {
      case e: Exception =>
        System.out.println("Error")
    }
  }

  // Function that extracts "author" tags from the given NodeList and calls context.write(K, V)
  def writeContext(context: Mapper[LongWritable, Text, Text, IntWritable]#Context, nList: NodeList) = {
    val one = new IntWritable(1)
    logger.debug("nList = " + nList)
    val range = 0 until (nList.getLength)
    // For every <article> tag encountered, parse <author> tags
    for (i <- range) {
      val nNode = nList.item(i)
      if (nNode.getNodeType == Node.ELEMENT_NODE) {
        val eElement = nNode.asInstanceOf[Element]

        val author = eElement.getElementsByTagName("author")
        val numOfAuthors = author.getLength
        // When only 1 <author> tag is found
        if (numOfAuthors == 1) {
          uicProfList.forEach(a => {
            if (a.contains(author.item(0).getTextContent)) {
              val profFirstValue = a(0)
              context.write(new Text(profFirstValue + "\t" + profFirstValue), one)
            }
          })
        }
        // When there are more than just 1 author
        if (numOfAuthors > 1) {
          val aList = new java.util.ArrayList[String]()
          val range = 0 until (numOfAuthors)

          for (i <- range) {
            //TODO Modify if condition to List.contains(by UIC Prof name) - DONE
            uicProfList.forEach(a => {
              if (a.contains(author.item(i).getTextContent)) {
                val profFirstValue = a(0)
                aList.add(profFirstValue)
              }
            })
          }
          // Sorting author names by alphabet
          Collections.sort(aList)
          val newRange = 0 until (aList.size())
          for (i <- newRange) {
            for (j <- (i + 1) to aList.size()) {
              // Setting context for every pair of professor
              // Also setting context for each professor to ensure we update total publications
              context.write(new Text(aList.get(i) + "\t" + aList.get(j)), one)
              context.write(new Text(aList.get(i) + "\t" + aList.get(i)), one)
              context.write(new Text(aList.get(j) + "\t" + aList.get(j)), one)
              logger.debug(aList.get(i) + "\t" + aList.get(j))
            }
          }
        }
      }
    }
  }
}