# Hadoop XML Map Reducer

This Hadoop Map Reduce has been written with the following Classes.
- XmlMapReduce: The Main class that Loads the configuration, creates Job and sets Mapper, Reducer and InputFormat classes
- MyMapper: Reads the XML file, breaks it into NodeList of values, processes the NodeList for "author" tag
- MyReducer: Reduces the Mapper output to give total articles
- XmlImputFormat: Custom Input Format class for reading XML file
- Utilities: Consists of helper functions for processing of data 

The splits are specified in the XmlImputFormat with the following instruction:
      FileInputFormat.setMaxInputSplitSize(job, 1280000000)
     

### Results

- Output format
The Professors are arranged in alphabetical order.
The output of MyReducer prints the following:
ProfessorName1 ProfessorName1 NumberOfArticles
ProfessorName1 ProfessorNameX NumberOfArticles

where ProfessorNameX indicates any professor Professor1 has collaborated with
The reason for this format is, to make it easier to convert into the necessary output file
Example:
A. Prasad Sistla	A. Prasad Sistla	19
A. Prasad Sistla	Bing Liu 0001	1
A. Prasad Sistla	Isabel F. Cruz	1
A. Prasad Sistla	Lenore D. Zuck	1
A. Prasad Sistla	Ouri Wolfson	8
A. Prasad Sistla	Robert H. Sloan	1

Here, Prof Sistla has a total of 19 articles and with each professor he has worked with, we print the number correspondingly.

- To open graph in Gephi:
1. Select CSV file format
2. Next
3. Select undirected graph
4. Spread nodes as required
5. Convert to text view
6. modify size of node from 0.5 to 4

- Analysis of the results:
The screenshots of the final graph generated can be found under "/GephiGraphs"

- [YouTube Link] () 

Without adding maxInputSplitSize, time taken = 4m 48s
On specifying maxInputSplitSize as 128000000, time taken = 4m 10s


### Prerequisites

Libraries and dependencies that you will need to run the code. 

1. [Hadoop-3.2.0](http://mirrors.ibiblio.org/apache/hadoop/common/hadoop-3.2.0/)
2. [Scala](https://www.scala-lang.org/download/)
3. [Scalalogging]()
4. [JUnit Test]()


### Installing

A step by step break down of how to get a development environment running

1. Download or clone repository by cloning using command - git clone git@bitbucket.org:lkarra2/lakshmi_manaswi_karra_cs441_hw2.git
2. Open in IntelliJ or move to project folder

```
Steps to run using SBT
1. clean compile 
2. run
```

```
Steps to run on IntelliJ
1. Open project as Scala Project
2. Build project
3. Run with arguments - <InputFolderPath> <OutputFolderPath>
```
To run from command line:
hadoop jar <jarFileName> <mainClassname> <AnyCommandLineArguements>
`bin/hadoop jar 
/Users/manaswikarra/CloudComputing/lakshmi_manaswi_karra_cs441_hw2/out/artifacts/lakshmi_manaswi_karra_cs441_hw2_jar/lakshmi_manaswi_karra_cs441_hw2.jar 
/Users/manaswikarra/CloudComputing/lakshmi_manaswi_karra_cs441_hw2/input 
/Users/manaswikarra/CloudComputing/lakshmi_manaswi_karra_cs441_hw2/project/target/output`

### Resources

The resources have been stored under /src/main/resources
The consist of
 -- mapred-site.xml
 -- professorNames.txt (Text File consisting of all the professor names)


## Running the tests

The tests 

To run the tests,
```
1. Run Tests on IntelliJ OR
2. run test - on SBT shell
```


## Built With

* [Cloudsim](http://www.cloudbus.org/cloudsim/) - CloudSim (Cloud Simulation) Toolkit for Modeling and Simulation of Clouds
* [Lightbend](https://github.com/lightbend/config) - Configuration library
* [SLF4J](https://www.slf4j.org/) - Logging Framework 
* [org.cloudbus.cloudsim.Log](~/lib/cloudsim-3.0.3.jar!/org/cloudbus/cloudsim/Log.class) - Printing Data logged by CloudSim 


## Authors

Lakshmi Manaswi Karra - 657276611 - [https://github.com/lkarra2]

## Acknowledgments

* Mark Grechanik for assigning this homework

